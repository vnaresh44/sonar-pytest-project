
def fibonacci(n):
    if n <= 1:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


def factorial(n):
    if n <= 1:
        return n
    else:
        return n * factorial(n - 1)


username = 'admin'
password = 'admin'  # Sensitive
usernamePassword = 'user=admin&password=admin'  # Sensitive

with open("../test.txt", "a") as f:  # Noncompliant
    pass

try:
    raise TypeError()
except ValueError or TypeError:  # Noncompliant
    print("Catching only ValueError")
except ValueError and TypeError:  # Noncompliant
    print("catching only TypeError")
except (ValueError or TypeError) as exception:  # Noncompliant
    print("Catching only ValueError")


# print('Error code %d' % '42')  # Noncompliant. Replace this value with a number as %d requires.
#
# print('User {1} is not allowed to perform this action'.format('Bob'))  # Noncompliant. Replaceme#nt field numbering should start at 0.
#
# print('User {0} has not been able to access {}'.format('Alice', 'MyFile'))  # Noncompliant. Use only manual or only automatic field numbering, don't mix them.
#
# print('User {a} has not been able to access {b}'.format(a='Alice'))  # Non

def adder(n):
    num = 0
    while num < n:
        yield num
        num += 1
    return num  # Noncompliant


# SSL.Context(SSL.SSLv3_METHOD)  # Noncompliant
import tempfile

filename = tempfile.mktemp() # Noncompliant
tmp_file = open(filename, "w+")

import ssl

ctx = ssl._create_unverified_context()
